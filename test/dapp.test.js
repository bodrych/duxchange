const utils = require('./utils')
const init = require('./init')

describe('dApp test suite', function () {
  let accounts, duck1, duck2

  before(async function () {
    ({ accounts } = await init())
  })

  beforeEach(async function () {
    let names = [['maker', 'DUCK-AAAAAAAA-GB'], ['taker', 'DUCK-BBBBBBBB-GB']]
    let promises = names.map(async ([account, name]) => {
      const issueTx = issue({ name, description: '', quantity: 1, decimals: 0, reissuable: false }, accounts.incubator)
      await broadcast(issueTx)
      await waitForTx(issueTx.id)
      const transferTx = transfer({ amount: 1, assetId: issueTx.id, recipient: address(accounts[account]) }, accounts.incubator)
      await broadcast(transferTx)
      await waitForTx(transferTx.id)
      return { account, name, id: issueTx.id }
    });
    [duck1, duck2] = await Promise.all(promises)
  })
  
  describe('valid duck', function () {
    it('all parameters', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'BBBBBBBB',
        color: 'B',
        generation: 'G',
        sterility: 1,
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        assetId: duck2.id,
        caller: accounts.taker,
      })
  
      expect(broadcast(acceptOffer)).to.be.ok
    })
    
    it('genotype', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'BBBBBBBB',
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        assetId: duck2.id,
        caller: accounts.taker,
      })
  
      expect(broadcast(acceptOffer)).to.be.ok
    })
    
    it('waves -> duck', async function () {
      const amount = 1e8
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'BBBBBBBB',
        assetId: null,
        amount,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const balanceBefore = await balance(address(accounts.taker))
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        assetId: duck2.id,
        caller: accounts.taker,
      })

      await broadcast(acceptOffer)
      await waitForTx(acceptOffer.id)
      const balanceAfter = await balance(address(accounts.taker))
      const delta = balanceAfter - (balanceBefore - utils.invokeFee - utils.serviceFee)
  
      expect(delta).to.equal(amount)
    })
  })
  
  describe('invalid duck', function () {
    it('genotype', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'CCCCCCCC',
        color: 'B',
        generation: 'G',
        sterility: 1,
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        assetId: duck2.id,
        caller: accounts.taker,
      })

      expect(broadcast(acceptOffer)).to.be.rejectedWith('Invalid genotype')
    })
    
    it('color', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        color: 'R',
        generation: 'G',
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        assetId: duck2.id,
        caller: accounts.taker,
      })

      expect(broadcast(acceptOffer)).to.be.rejectedWith('Invalid color')
    })
    
    it('sterility', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        sterility: 2,
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        assetId: duck2.id,
        caller: accounts.taker,
      })

      expect(broadcast(acceptOffer)).to.be.rejectedWith('Invalid sterility')
    })
    
    it('generation', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        generation: 'L',
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        assetId: duck2.id,
        caller: accounts.taker,
      })

      expect(broadcast(acceptOffer)).to.be.rejectedWith('Invalid generation')
    })
  })

  describe('invalid payment', function () {
    it('issuer', async function () {
      const issueTx = issue({ name: 'DUCK-AAAAAAAA-JU', description: '', quantity: 1, decimals: 0, reissuable: false }, accounts.maker)
      await broadcast(issueTx)
      await waitForTx(issueTx.id)
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        assetId: issueTx.id,
        caller: accounts.maker,
      })

      expect(broadcast(makeOffer)).to.be.rejectedWith('This is not a duck!')
    })
  })

  describe('invalid fee', function () {
    it('asset', async function () {
      const issueTx = issue({ name: 'fake', description: '', quantity: 1e16, decimals: 8, reissuable: true }, accounts.maker)
      await broadcast(issueTx)
      await waitForTx(issueTx.id)
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        assetId: duck1.id,
        feeAssetId: issueTx.id,
        caller: accounts.maker,
      })

      expect(broadcast(makeOffer)).to.be.rejectedWith('Fee is 0.01 WAVES')
    })

    it('amount', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        assetId: duck1.id,
        feeAmount: utils.serviceFee - 1,
        caller: accounts.maker,
      })

      expect(broadcast(makeOffer)).to.be.rejectedWith('Fee is 0.01 WAVES')
    })
  })

  describe('cancel', function () {
    it('by the owner', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const cancelOffer = utils.cancelOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        caller: accounts.maker,
      })
  
      expect(broadcast(cancelOffer)).to.be.ok
    })

    it('not by the owner', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const cancelOffer = utils.cancelOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        caller: accounts.taker,
      })
  
      expect(broadcast(cancelOffer)).to.be.rejectedWith('Only offer owner can cancel it')
    })
  })

  describe('password', function () {
    it('valid', async function () {
      const password = 'test'
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'BBBBBBBB',
        password: publicKey(password),
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        signature: signBytes({ privateKey: privateKey(password) }, publicKey(accounts.taker)),
        assetId: duck2.id,
        caller: accounts.taker,
      })

      expect(broadcast(acceptOffer)).to.be.ok
    })
    
    it('invalid', async function () {
      const password = 'test'
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'BBBBBBBB',
        password: publicKey(password),
        assetId: duck1.id,
        caller: accounts.maker,
      })
      await broadcast(makeOffer)
      await waitForTx(makeOffer.id)
      const acceptOffer = utils.acceptOffer({
        dApp: address(accounts.duxchange),
        offerId: `offer_${makeOffer.id}`,
        signature: '',
        assetId: duck2.id,
        caller: accounts.taker,
      })

      expect(broadcast(acceptOffer)).to.be.rejectedWith('Invalid password')
    })
  })

  describe('invalid format', function () {
    it('genotype (delimiter)', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'BB_BBBBB',
        assetId: duck1.id,
        caller: accounts.maker,
      })
  
      expect(broadcast(makeOffer)).to.be.rejectedWith('Invalid genotype format')
    })

    it('genotype (size)', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        genotype: 'BBBBBBB',
        assetId: duck1.id,
        caller: accounts.maker,
      })
  
      expect(broadcast(makeOffer)).to.be.rejectedWith('Invalid genotype format')
    })

    it('sterility', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        sterility: 3,
        assetId: duck1.id,
        caller: accounts.maker,
      })
  
      expect(broadcast(makeOffer)).to.be.rejectedWith('Invalid sterility format')
    })

    it('password', async function () {
      const makeOffer = utils.makeOffer({
        dApp: address(accounts.duxchange),
        password: 'te_st',
        assetId: duck1.id,
        caller: accounts.maker,
      })
  
      expect(broadcast(makeOffer)).to.be.rejectedWith('Error while executing account-script: can\'t parse Base58 string')
    })
  })
})
