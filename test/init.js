module.exports = async (accounts = null) => {
  const adminPublicKey = '5584brtd1tLqMNJdCzvPFUEL1ujYRyi4Sy3zEZ5nTPsJ'
  const incubatorAddress = '3PEktVux2RhchSN63DsDo4b4mz4QqzKSeDv'
  const breederAddress = '3PDVuU45H7Eh5dmtNbnRNRStGwULA7NY6Hb'

  const names = ['duxchange', 'taker', 'maker', 'admin', 'incubator', 'breeder']
  accounts = accounts || Object.fromEntries(names.map(item => [item, wavesCrypto.randomSeed(5)]))
  console.table(Object.entries(accounts).map(([key, value]) => [key, value, address(value)]))
  const mtt = massTransfer({
    transfers: Object.values(accounts).map(item => ({ recipient: address(item), amount: 1e10 }))
  })
  await broadcast(mtt)
  await waitForTx(mtt.id)

  const deploy = async () => {
    const script = file('dapp.ride')
      .replace(adminPublicKey, publicKey(accounts.admin))
      .replace(incubatorAddress, address(accounts.incubator))
      .replace(breederAddress, address(accounts.breeder))
    const compiled = compile(script)
    const ssTx = setScript({ script: compiled }, accounts.duxchange)
    broadcast(ssTx)
    await waitForTx(ssTx.id)
  }

  await deploy()

  return { accounts }
}
