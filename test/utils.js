const serviceFee = 1e6
const invokeFee = 5e5

const makeOffer = ({ dApp, genotype = '', color = '', generation = '', sterility = 0, password = '', assetId, amount = 1, feeAssetId = null, feeAmount = serviceFee, caller }) => invokeScript({
  dApp,
  call: {
    function: 'makeOffer',
    args: [
      { type: 'string', value: genotype },
      { type: 'string', value: color },
      { type: 'string', value: generation },
      { type: 'integer', value: sterility },
      { type: 'string', value: password },
    ],
  },
  payment: [{ assetId, amount }, { assetId: feeAssetId, amount: feeAmount }],
}, caller)

const acceptOffer = ({ dApp, offerId, signature = '', assetId, amount = 1, feeAssetId = null, feeAmount = serviceFee, caller }) => invokeScript({
  dApp,
  call: {
    function: 'acceptOffer',
    args: [
      { type: 'string', value: offerId },
      { type: 'string', value: signature },
    ],
  },
  payment: [{ assetId, amount }, { assetId: feeAssetId, amount: feeAmount }],
}, caller)

const cancelOffer = ({ dApp, offerId, caller }) => invokeScript({
  dApp,
  call: {
    function: 'cancelOffer',
    args: [
      { type: 'string', value: offerId },
    ],
  },
  payment: [],
}, caller)

module.exports = {
  serviceFee,
  invokeFee,
  makeOffer,
  acceptOffer,
  cancelOffer,
}
