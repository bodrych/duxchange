import { rest } from 'msw'

const apiBase = process.env.VUE_APP_NODE_URL!
const dApp = process.env.VUE_APP_DAPP_ADDRESS!

export const handlers = [
  rest.get(`${apiBase}addresses/data/${dApp}`, async (req, res, ctx) => {
    await new Promise(resolve => setTimeout(resolve, 1000))
    return res(
      ctx.status(200),
      ctx.json([
        {
          key: 'offer_FoiudK74yYwjQxdMeQXEsyQwnhLwBMrFEmX8E3Uwugx9',
          type: 'string',
          value: '3P96VpN2dBS1111111ydADa5a6AD1Un9iUv_WAVES_100000000____0_'
        },
        {
          key: 'offer_AoiudK74yYwjQxdMeQXEsyQwnhLwBMrFEmX8E3Uwugx8',
          type: 'string',
          value: '3P96VpN2dBS1111111ydADa5a6AD1Un9iUv_WAVES_90000000___G_0_'
        },
        {
          key: 'offer_BoiudK74yYwjQxdMeQXEsyQwnhLwBMrFEmX8E3Uwugx7',
          type: 'string',
          value: '3P96VpN2dBS1111111ydADa5a6AD1Un9iUv_duck1_1__R__1_'
        },
      ]),
    )
  }),
  rest.get(`${apiBase}assets/details`, async (req, res, ctx) => {
    await new Promise(resolve => setTimeout(resolve, 1000))
    const id = req.url.searchParams.get('id')
    return res(
      ctx.status(200),
      ctx.json([
        {
          assetId: id,
          name: `DUCK-${id}`,
          decimals: 0,
          reissuable: false,
          quantity: 1,
        }
      ]),
    )
  }),
]
