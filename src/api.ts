import { nodeInteraction, waitForTx } from '@waves/waves-transactions'
import { IStringData, TRANSACTION_TYPE } from '@waves/waves-transactions/dist/transactions'
import { MakeOfferParams, Offers, CancelOfferParams, AcceptOfferParams, SterilityStatus } from '../types'
import * as wc from '@waves/ts-lib-crypto'
import { create as createApi } from '@waves/node-api-js'
import { TAssetDetails } from '@waves/node-api-js/es/api-node/assets'

const apiBase = process.env.VUE_APP_NODE_URL!
const api = createApi(apiBase)

const dApp = process.env.VUE_APP_DAPP_ADDRESS!
const serviceFee = 1e6

export const makeOffer = async ({
  genotype = '',
  color = '',
  generation = '',
  sterility = 0,
  password = '',
  assetId,
  amount = 1,
}: MakeOfferParams) => {
  const makeOfferData: WavesKeeper.TScriptInvocationTxData = {
    type: TRANSACTION_TYPE.INVOKE_SCRIPT,
    data: {
      dApp,
      call: {
        function: 'makeOffer',
        args: [
          { type: 'string', value: genotype },
          { type: 'string', value: color },
          { type: 'string', value: generation },
          { type: 'integer', value: sterility },
          { type: 'string', value: password ? wc.publicKey(password) : '' },
        ],
      },
      payment: [{ assetId: assetId || 'WAVES', tokens: amount || 0 }, { assetId: 'WAVES', amount: serviceFee }],
      fee: { assetId: 'WAVES', amount: 500000 },
    },
  }
  try {
    const response = await window.WavesKeeper.signAndPublishTransaction(makeOfferData)
    const data = JSON.parse(response)
    await waitForTx(data.id, { apiBase }, { credentials: 'omit' })
    return response
  } catch (e) {
    throw e
  }
}

export const acceptOffer = async ({ offerId, password = '', assetId, amount = 1 }: AcceptOfferParams) => {
  const publicKey = (await WavesKeeper.publicState()).account?.publicKey!
  const acceptOfferData: WavesKeeper.TScriptInvocationTxData = {
    type: TRANSACTION_TYPE.INVOKE_SCRIPT,
    data: {
      dApp,
      call: {
        function: 'acceptOffer',
        args: [
          { type: 'string', value: `offer_${offerId}` },
          { type: 'string', value: password ? wc.signBytes({ privateKey: wc.privateKey(password) }, publicKey) : '' },
        ],
      },
      payment: [{ assetId, amount }, { assetId: 'WAVES', amount: serviceFee }],
      fee: { assetId: 'WAVES', coins: 500000 },
    },
  }
  try {
    const response = await window.WavesKeeper.signAndPublishTransaction(acceptOfferData)
    const data = JSON.parse(response)
    await waitForTx(data.id, { apiBase }, { credentials: 'omit' })
    return response
  } catch (e) {
    throw e
  }
}

export const cancelOffer = async ({ offerId }: CancelOfferParams) => {
  const cancelOfferData: WavesKeeper.TScriptInvocationTxData = {
    type: TRANSACTION_TYPE.INVOKE_SCRIPT,
    data: {
      dApp,
      call: {
        function: 'cancelOffer',
        args: [
          { type: 'string', value: `offer_${offerId}` },
        ],
      },
      payment: [],
      fee: { assetId: 'WAVES', coins: 500000 },
    },
  }
  try {
    const response = await window.WavesKeeper.signAndPublishTransaction(cancelOfferData)
    const data = JSON.parse(response)
    await waitForTx(data.id, { apiBase }, { credentials: 'omit' })
    return response
  } catch (e) {
    throw e
  }
}

export const fetchAssetsDetails = async (assets: string[]): Promise<TAssetDetails[]> => {
  return (await api.assets.fetchAssetsDetails(assets, { credentials: 'omit' })).filter(item => 'assetId' in item) as TAssetDetails[]
}

export const fetchOffers = async (): Promise<Offers> => {
  const data = <Record<string, IStringData>>(
    await nodeInteraction.accountData(
      { address: dApp, match: 'offer.+' },
      process.env.VUE_APP_NODE_URL!,
      { credentials: 'omit' },
    )
  )
  // offer_<txId>: <owner>_<payment.assetId>_<payment.amount>_<genotype>_<color>_<generation>_<sterility>_<password>
  return Object.fromEntries(
    Object.entries(data).map(([key, value]) => {
      const offerId = key.split('_')[1]
      const [
        owner,
        assetId,
        amount,
        genotype,
        color,
        generation,
        sterility,
        password,
      ] = value.value.split('_')
      const sterilityStatus = parseInt(sterility, 10) as SterilityStatus
      return [
        offerId,
        {
          offerId,
          owner,
          assetId,
          amount: parseInt(amount, 10),
          genotype,
          color,
          generation,
          sterility: sterilityStatus,
          password,
        },
      ]
    }),
  )
}
