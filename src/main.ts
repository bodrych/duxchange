import { createApp } from 'vue'
import App from './App.vue'
import VueClickAway from 'vue3-click-away'

const app = createApp(App)

app.use(VueClickAway)

if (process.env.NODE_ENV === 'development') {
  import('@/mocks/browser').then(async ({ worker }) => {
    await worker.start()
    app.mount('#app')
  })
} else {
  app.mount('#app')
}
