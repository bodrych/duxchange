import { AssetDecimals } from '@waves/ts-types/src/parts'

export type SterilityStatus = 0 | 1 | 2

export interface Offer {
  offerId: string,
  owner: string,
  assetId: string,
  amount: number,
  genotype: string,
  color: string,
  generation: string,
  sterility: SterilityStatus,
  password: string,
  name?: string,
}

export interface Offers {
  [offerId: string]: Offer,
}

export interface MakeOfferParams {
  genotype: string,
  color: string,
  generation: string,
  sterility: SterilityStatus,
  password: string,
  assetId: string,
  amount: number | null,
}

export interface AcceptOfferParams {
  offerId: string,
  password: string,
  assetId: string,
  amount: number,
}

export interface CancelOfferParams {
  offerId: string,
}

export interface AssetDetails {
  [id: string]: {
    assetId: string,
    name: string,
    decimals: AssetDecimals,
  }
}
