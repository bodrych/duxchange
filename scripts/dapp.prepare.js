const init = require('../test/init')

const prepare = async () => {
  let accounts = {
    duxchange: 'year punch apart album cousin',
    taker: 'pistol lesson galaxy exist ask',
    maker: 'peace yard genius annual film',
    admin: 'grunt bike fashion ethics barrel',
    incubator: 'balcony dolphin earn utility license',
    breeder: 'strategy gap whip lounge scale',
  }
  await init(accounts)
  let duckNames = {
    maker: ['DUCK-AAAAAAAA-GB', 'DUCK-CCCCCCCC-GY', 'DUCK-EEEEEEEE-GR'],
    taker: ['DUCK-BBBBBBBB-GR', 'DUCK-DDDDDDDD-GB', 'DUCK-FFFFFFFF-GY']
  }
  for (let [account, names] of Object.entries(duckNames)) {
    const promises = names.map(async name => {
      const issueTx = issue({
        name,
        description: '',
        quantity: 1,
        decimals: 0,
        reissuable: false,
      }, accounts.incubator)
      await broadcast(issueTx)
      await waitForTx(issueTx.id)
      const transferTx = transfer({
        amount: 1,
        assetId: issueTx.id,
        recipient: address(accounts[account])
      }, accounts.incubator)
      await broadcast(transferTx)
      return { account, name, id: issueTx.id }
    })
    await Promise.all(promises)
  }
}

prepare().catch(console.log)
