{-# STDLIB_VERSION 4 #-}
{-# CONTENT_TYPE DAPP #-}
{-# SCRIPT_TYPE ACCOUNT #-}

# duxchange

# Offer DataEntry format:
# offer_<txId>: <owner>_<payment.assetId>_<payment.amount>_<genotype>_<color>_<generation>_<sterility>_<password>

# Service fee: 0.01 WAVES

let admin = base58'5584brtd1tLqMNJdCzvPFUEL1ujYRyi4Sy3zEZ5nTPsJ'.addressFromPublicKey()

let incubatorAddress = base58'3PEktVux2RhchSN63DsDo4b4mz4QqzKSeDv'.Address()
let breederAddress = base58'3PDVuU45H7Eh5dmtNbnRNRStGwULA7NY6Hb'.Address()

let serviceFee = 1000000

let genotypeStringSize = 8
let colorStringSize = 1
let generationStringSize = 1

let sterilityStatusNotImportant = 0
let sterilityStatusCanBreed = 1
let sterilityStatusSterile = 2

let sterilityStatuses = nil :+ sterilityStatusNotImportant :+ sterilityStatusCanBreed :+ sterilityStatusSterile

let delimiter = "_"

func checkFee(i: Invocation) = {
    let fee = i.payments[1].valueOrErrorMessage("Where is fee?")
    (!fee.assetId.isDefined() && fee.amount >= serviceFee) || throw("Fee is 0.01 WAVES")
}

func checkPayment(i: Invocation, wavesAllowed: Boolean) = {
    match i.payments[0].valueOrErrorMessage("Where is payment?").assetId {
        case _: Unit => wavesAllowed || throw("Only ducks allowed")
        case a: ByteVector => (a.assetInfo().value().issuer == incubatorAddress || a.assetInfo().value().issuer == breederAddress) || throw("This is not a duck!")
    }
}

func checkSterility(assetId: String, sterility: Int, message: String) = {
    let assetSterility = match breederAddress.getInteger("asset_" + assetId + "_children") {
        case n: Int => if (n > 0) then sterilityStatusSterile else sterilityStatusCanBreed
        case _: Unit => sterilityStatusCanBreed
    }
    (sterility == sterilityStatusNotImportant || sterility == assetSterility) || throw(message)
}

@Callable(i)
func makeOffer(genotype: String, color: String, generation: String, sterility: Int, password: String) = {
    let validations = nil
        :+ checkFee(i)
        :+ checkPayment(i, true)
        :+ (genotype == "" || (genotype.size() == genotypeStringSize && !genotype.contains(delimiter)) || throw("Invalid genotype format"))
        :+ (color == "" ||(color.size() == colorStringSize && !color.contains(delimiter)) || throw("Invalid color format"))
        :+ (generation == "" || (generation.size() == generationStringSize && !generation.contains(delimiter)) || throw("Invalid generation format"))
        :+ (sterilityStatuses.containsElement(sterility) || throw("Invalid sterility format"))
        :+ (password == "" || password.fromBase58String().size() > 0 || throw("Invalid password format"))

    if (validations.containsElement(false)) then throw() else
        let owner = i.caller.bytes.toBase58String()
        let offerId = "offer" + delimiter + i.transactionId.toBase58String()
        let payment = i.payments[0].value()
        let assetId = match payment.assetId {
            case _: Unit => "WAVES"
            case a: ByteVector => a.toBase58String()
        }
        let offerData = owner
            + delimiter + assetId
            + delimiter + payment.amount.toString()
            + delimiter + genotype
            + delimiter + color
            + delimiter + generation
            + delimiter + sterility.toString()
            + delimiter + password.fromBase58String().toBase58String()
        let offerDataEntry = StringEntry(offerId, offerData)
        let fee = i.payments[1].value()
        let toAdmin = ScriptTransfer(admin, fee.amount, fee.assetId)

        nil :+ offerDataEntry :+ toAdmin
}

@Callable(i)
func acceptOffer(offerId: String, signature: String) = {
    let offerData = this.getString(offerId).valueOrErrorMessage("Invalid offer id")
    let offerDataParts = offerData.split(delimiter)
    let offerOwner = offerDataParts[0]
    let offerAssetId = if (offerDataParts[1] == "WAVES") then unit else offerDataParts[1].fromBase58String()
    let offerAssetAmount = offerDataParts[2].parseIntValue()
    let offerGenotype = offerDataParts[3]
    let offerColor = offerDataParts[4]
    let offerGeneration = offerDataParts[5]
    let offerSterility = offerDataParts[6].parseIntValue()
    let offerPassword = offerDataParts[7]
    if (!checkPayment(i, false)) then throw() else
        let info = i.payments[0].value().assetId.value().assetInfo().value()
        let name = info.name
        let nameParts = name.split("-")
        let genotype = nameParts[1]
        let generation = nameParts[2].split("")[0]
        let color = nameParts[2].split("")[1]
        let validations = nil
            :+ ((offerPassword == "" || sigVerify(i.callerPublicKey, signature.fromBase58String(), offerPassword.fromBase58String())) || throw("Invalid password"))
            :+ checkFee(i)
            :+ ((offerGenotype == "" || offerGenotype == genotype) || throw("Invalid genotype"))
            :+ ((offerColor == "" || offerColor == color) || throw("Invalid color"))
            :+ ((offerGeneration == "" || offerGeneration == generation) || throw("Invalid generation"))
            :+ checkSterility(info.id.toBase58String(), offerSterility, "Invalid sterility")

        if (validations.containsElement(false)) then throw() else
            let payment = i.payments[0].value()
            let toOwner = ScriptTransfer(offerOwner.addressFromString().value(), payment.amount, payment.assetId)
            let toCaller = ScriptTransfer(i.caller, offerAssetAmount, offerAssetId)
            let fee = i.payments[1].value()
            let toAdmin = ScriptTransfer(admin, fee.amount, fee.assetId)
            let offerDeleteEntry = DeleteEntry(offerId)

            nil :+ toOwner :+ toCaller :+ toAdmin :+ offerDeleteEntry
}

@Callable(i)
func cancelOffer(offerId: String) = {
    let offerData = this.getString(offerId).valueOrErrorMessage("Invalid offer id")
    let offerDataParts = offerData.split(delimiter)
    let offerOwner = offerDataParts[0].addressFromString().value()
    let offerAssetId = if (offerDataParts[1] == "WAVES") then unit else offerDataParts[1].fromBase58String()
    let offerAssetAmount = offerDataParts[2].parseIntValue()
    if (i.caller != offerOwner) then throw("Only offer owner can cancel it") else
        let toOwner = ScriptTransfer(offerOwner, offerAssetAmount, offerAssetId)
        let offerDeleteEntry = DeleteEntry(offerId)
        nil :+ offerDeleteEntry :+ toOwner
}

# @Verifier(tx)
# func verify() = {
#     false
# }
